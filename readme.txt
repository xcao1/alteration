===============================================================================
================                  ALTERATION                   ================
===============================================================================
Developing Tips :

12/12/9     Game has already been deplyed on Google Play
            link to the website :

            https://play.google.com/store/apps/details?
            id=org.cocos2dx.test01&feature=search_result#?
            t=W251bGwsMSwyLDEsIm9yZy5jb2NvczJkeC50ZXN0MDEiXQ..

12/11/29    Changed art style.Made the game more interesting.

12/11/11    Finished basic game loop.The game now can be played between three 
            basic states.

12/11/8     Designning alpha version.Deleted a lot of files.These fies are 
            still useful.The reason for deleting them is convenient for 
            debugging.These files will be added again later.
			
12/10/21    The tutorial is very very misleading.It should seperate the Hello-
            World class into two classes.One is a subclass of CCLayer, another
            is a subclass of CCScene.Since I was misleaded by it, the archite-
            cture of the game will be redesigned.
===============================================================================
GAME TYPE :
The game is a 2D Turn-Based Strategy war chess game.

PLATFORM :
[Android]  [Win32]

GAME ENGINE :
The game is built with cocos2d-x game engine.

LANGUAGE :
C++
===============================================================================
1.For details about game design, please read the GDD.
2.For details about the technology, please read the TDD.
3.Since the game is being developing, please read the Change logs as well.
===============================================================================
The canvas(Java) version was abandoned.