LOCAL_PATH := $(call my-dir)

include $(CLEAR_VARS)

LOCAL_MODULE := game_shared

LOCAL_MODULE_FILENAME := libgame

LOCAL_SRC_FILES := hellocpp/main.cpp \
                   ../../Classes/AppDelegate.cpp \
                   ../../Classes/Act.cpp \
				   ../../Classes/Actor.cpp \
				   ../../Classes/DrawArea.cpp \
				   ../../Classes/DrawGrids.cpp \
				   ../../Classes/MenuScene.cpp \
				   ../../Classes/PlayingScene.cpp \
				   ../../Classes/State.cpp \
				   ../../Classes/StateMachine.cpp \
				   ../../Classes/TurnCount.cpp \
				   ../../Classes/UnitSelect.cpp \
				   ../../Classes/GridMap.cpp \
				   ../../Classes/Menu.cpp \
				   ../../Classes/DrawAdvancedArea.cpp \
				   ../../Classes/Player.cpp \
				   ../../Classes/SimpleAI.cpp
                   
LOCAL_C_INCLUDES := $(LOCAL_PATH)/../../Classes                   

LOCAL_WHOLE_STATIC_LIBRARIES := cocos2dx_static cocosdenshion_static cocos_extension_static
            
include $(BUILD_SHARED_LIBRARY)

$(call import-module,CocosDenshion/android) \
$(call import-module,cocos2dx) \
$(call import-module,extensions)
