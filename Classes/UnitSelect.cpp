#include "UnitSelect.h"
#include "Player.h"
#include <cmath>
UnitSelect::UnitSelect( PlayingScene *scene ):State(scene)
{
	smallTurnsUsed ++;
	if( smallTurnsUsed == 0)
		mPlayer = PlayerOne;
	else
	{
		if( mPlayer == PlayerTwo )
			mPlayer = PlayerOne;
		else
			mPlayer = PlayerTwo;
	}
	ended = false;
	nextName = ACT;

	// =======================================
	// if current player has no active unit
	// check if another player has active unit
	// if have, change to that player,
	// else, end this turn
	bool anyActive = false;
	vector<Actor>::iterator iter;
	if( mPlayer == PlayerOne )
	{
		anyActive = mScene->one->anyActive();
		if( anyActive == false )
		{
			anyActive = mScene->two->anyActive();
			if( anyActive == true )
				mPlayer = PlayerTwo;
			else
			{
				// because at the beginning the player will be changed
				// we want to still use this player
				mPlayer = PlayerTwo;
				nextName = TURNCOUNT;
				ended = true;
			}
		}
	}
	else if( mPlayer == PlayerTwo )
	{
		anyActive = mScene->two->anyActive();
		if( anyActive == false )
		{
			anyActive = mScene->one->anyActive();
			if( anyActive == true )
				mPlayer = PlayerOne;
			else
			{
				// because at the beginning the player will be changed
				// we want to still use this player
				mPlayer = PlayerOne;
				nextName = TURNCOUNT;
				ended = true;
			}
		}
	}
	State::updateCamera();
}
UnitSelect::~UnitSelect()
{
}

void UnitSelect::onTouchesBegan( CCSet *touches, CCEvent *event )
{
	moveCamera = false;
	State::onTouchesBegan( touches, event );
}
void UnitSelect::onTouchesMoved( CCSet *touches, CCEvent *event )
{
	State::onTouchesMoved( touches, event );
}
void UnitSelect::onTouchesEnded( CCSet *touches, CCEvent *event )
{
	State::onTouchesEnded(touches, event);
	if( !( moveCamera || checkUnits || mainMenuUp || resuming ))
	{
		CCTouch *touch = ( CCTouch * )( touches->anyObject() );
		CCPoint location = touch->locationInView();
		location = CCDirector::sharedDirector()->convertToGL(location);
		CCPoint origin = mScene->mInteractiveScenes->getPosition();
		int x = fabs(location.x-origin.x)/mScene->mMap->gridSize;
		int y = fabs(location.y-origin.y)/mScene->mMap->gridSize;
		
					
		if( mPlayer == PlayerOne )
		{
			if( Actor *target = mScene->one->getActorAtPosition(x,y) )
			{
				if( target->isActive() )
				{
					target->selected = true;
					ended = true;
					nextName = ACT;		
				}
			}
		}
		else if( mPlayer == PlayerTwo )
		{
			if( !mScene->two->mIsAI )
			{
				if( Actor *target = mScene->two->getActorAtPosition(x,y) )
				{
					if( target->isActive() )
					{
						target->selected = true;
						ended = true;
						nextName = ACT;		
					}
				}
			}
		}
	}			
}
void UnitSelect::onUpdate( float dt )
{
	State::onUpdate( dt );
	if( mPlayer == PlayerTwo )
		if( mScene->two->mIsAI )
		{
			Actor *target = 0;
			if( target = mScene->two->getActiveActor() )
			{
				target->selected = true;
				ended = true;
				nextName = AI;
			}
			else
			{
				nextName = TURNCOUNT;
				ended = true;
			}
		}
}
void UnitSelect::onMenu(CCObject* pSender )
{
}