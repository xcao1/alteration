#ifndef __PLAYER_H__
#define __PLAYER_H__
#include "Actor.h"
#include <vector>
using namespace std;
class PlayingScene;
class Player
{
private:
	PlayingScene *mScene;
public:
	// player one or player two
	bool mSide;// true - Player1, false - Player2

	bool mIsAI;
	vector<Actor> mArmy;
	int mScore;


	Player( PlayingScene *scene, int score, bool isAI, bool side );
	~Player();

	void add( int x, int y );
	Actor *getSelectedActor();
	Actor *getActorAtPosition( int x, int y );
	Actor *getActiveActor();
	Actor *getInDangerActor();
	Actor *getRandomActor();

	void deactivateAll();

	bool onAttack(Actor *attacker, int x, int y );
	bool isEmpty();
	void refresh();
	void recover();
	void levelUp();
	bool anyActive();

	void onUpdate();

};
#endif // !__PLAYER_H__
