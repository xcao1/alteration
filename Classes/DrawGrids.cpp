#include "DrawGrids.h"

DrawGrids::DrawGrids():gridSize(0),rowX(0),rowY(0)
{
}
DrawGrids::DrawGrids( int rX, int rY, float size ):rowX(rX),rowY(rY),gridSize(size)
{
}
DrawGrids::~DrawGrids()
{
}

void DrawGrids::draw()
{
	for( int i = 0; i != ( rowX + 1 ); i ++ )
	{// rowX
		ccDrawLine( ccp( i * gridSize, 0 ), ccp( i * gridSize, rowY * gridSize ) );
	}
	for( int i = 0; i != ( rowY + 1 ); i ++ )
	{// rowY
		ccDrawLine( ccp( 0, i * gridSize ), ccp( rowX * gridSize, i * gridSize ) );
	}
}
void DrawGrids::setRows( int rX, int rY )
{
	rowX = rX;
	rowY = rY;
}
void DrawGrids::setGridSize( float size )
{
	gridSize = size;
}