#ifndef __DRAW_AREA_H__
#define __DRAW_AREA_H__
#include "cocos2d.h"

using namespace cocos2d;
class DrawArea:public CCNode
{
private:
	int position[2];
	float gridSize;
	float range;
	int type;// 0 - move, 1 - attack
public:
	DrawArea( float size );
	~DrawArea();

	void draw(void);
	void set( int (&pos)[2], int num, int t );
	void set( int x, int y, int num, int t );
	void setGridSize( float size );
};
#endif // !__DRAW_AREA_H__
