#ifndef __SIMPLE_AI_H__
#define __SIMPLE_AI_H__
#include "State.h"

class PlayingScene;
class Actor;
class SimpleAI:public State
{
private:
	bool killOrEscape;// true - kill, false - escape
	Actor *selectedActor;
public:
	SimpleAI(PlayingScene *scene);
	~SimpleAI();
	void onTouchesBegan( CCSet *touches, CCEvent *event );
	void onTouchesMoved( CCSet *touches, CCEvent *event );
	void onTouchesEnded( CCSet *touches, CCEvent *event );
	void onUpdate( float dt );
    void onMenu(CCObject* pSender );

	void attackEnemyInRange();
	bool plan();
	void escape();
	void kill();
	Actor *getTarget();
	
};
#endif // !__SIMPLE_AI_H__
