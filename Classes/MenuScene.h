#ifndef __MENU_SCENE_H__
#define __MENU_SCENE_H__

#include "cocos2d.h"
#include "SimpleAudioEngine.h"
using namespace cocos2d;
class MenuScene : public CCLayer
{
private:
	CCSprite *mBackground;
	CCSprite *mName;
	bool state;// true - start, false - end
public:
	CCSprite *mVsAI;
	CCSprite *mVsHuman;
	CCSprite *mGameOver;

	CCLabelTTF* printScore;


	MenuScene();
	~MenuScene();

	void setState(bool beginOrEnd);
	// =================================================
	// COCOS2D-X architexture
	// there's no 'id' in cpp, so we recommand to return the exactly class pointer
    static CCScene* scene();
	// implement the "static node()" method manually
    CREATE_FUNC(MenuScene);
    // Here's a difference. Method 'init' in cocos2d-x returns bool, instead of returning 'id' in cocos2d-iphone
    virtual bool init();
    void ccTouchesBegan(CCSet *touches, CCEvent *event);
    void ccTouchesMoved(CCSet *touches, CCEvent *event);
    void ccTouchesEnded(CCSet *touches, CCEvent *event);
    void menuCloseCallback(CCObject* pSender);



};

#endif  // __MENU_SCENE_H__