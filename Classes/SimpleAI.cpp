#include "SimpleAI.h"
#include "PlayingScene.h"
#include "MenuScene.h"
#include "GridMap.h"
#include "DrawAdvancedArea.h"
#include "DrawArea.h"
#include "Actor.h"
#include "Player.h"
#include <vector>
#include <cmath>
SimpleAI::SimpleAI(PlayingScene *scene):State(scene),killOrEscape(true)
{
	ended = false;
	nextName = UNITSELECT;

	// ==========================
	// store the selected units into a member pointer
	selectedActor = mScene->two->getSelectedActor();

	setCamera(selectedActor->gridPosition[1]);
}
SimpleAI::~SimpleAI()
{
}

bool SimpleAI::plan()
{
	float value = 60;
	float weightEscape = -1000;
	float weightKill = 600;
	float weightQuantities = -10;


	int sizeEnemy = mScene->one->mArmy.size();
	int sizeMe = mScene->two->mArmy.size();
	float quantities = sizeEnemy - sizeMe;
	value += quantities * weightQuantities;
	if( selectedActor->health < 0.5*selectedActor->maxHealth)
		value += weightEscape;
	int range = selectedActor->SPEED+selectedActor->RANGE;
	int mX = selectedActor->gridPosition[0];
	int mY = selectedActor->gridPosition[1];
	
	vector<Actor>::iterator iter = mScene->one->mArmy.begin();
	while( iter != mScene->one->mArmy.end() )
	{
		float distance = fabs((float)(iter->gridPosition[0] - mX)) + fabs((float)(iter->gridPosition[1] - mY));
		if( distance <= range )
			if( iter->health < 0.2*iter->maxHealth )
				value += weightKill;
		iter ++;
	}
	if( value >= 0 )
		killOrEscape = true;
	else
		killOrEscape = false;
	return killOrEscape;
}
void SimpleAI::attackEnemyInRange()
{
	if( selectedActor->attackChance > 0 )
	{
		// search if there is enemy in range
		vector<Actor>::iterator iter = mScene->one->mArmy.begin();
		int size = mScene->one->mArmy.size();
		while( iter != mScene->one->mArmy.end() )
		{
			int distance = fabs((float)(iter->gridPosition[0]-selectedActor->gridPosition[0]))+
							fabs((float)(iter->gridPosition[1]-selectedActor->gridPosition[1]));
			if(distance <= selectedActor->RANGE)
			{// attack
				selectedActor->attackChance--;
				mScene->one->onAttack(selectedActor,iter->gridPosition[0],iter->gridPosition[1]);
				// check if win
				if( mScene->one->isEmpty() )
				{// win
					mScene->gameOVer();
				}
			}
			if( mScene->one->mArmy.size() == size )
				iter ++;
			else
				break;
		}
	}
}
void SimpleAI::escape()
{
	attackEnemyInRange();
	float weightHealth = -20;
	float weightDistance = 10;
	vector<int> scores;
	mScene->mMap->calculateMoveArea(selectedActor->gridPosition[0], selectedActor->gridPosition[1],selectedActor->SPEED);
	for(int i = 0; i != mScene->mMap->rowX; i ++)
		for(int j = 0; j != mScene->mMap->rowY; j ++)
		{
			if( mScene->mMap->moveArea[i][j] == 0 )
			{// for each position
				float scoreOfThisPosition = 0;
				// ====================================================================================
				// for each posible move position
				// give it a score, 
				// chose a position that has the highest score 
				// the score is evaluated as below :
				// the sum of distance to each enemy units, 
				// the further the safer
				// and different enemy units may also has different weight based on their health
				// the lower the health, the more probably the enemy will not attack
				//
				// scoreOfEnemy1 = enemy1.health*weightHealth + enemy1.distanceToMe*weightDistance
				// weightHealth is negtive
				// weightDistance is positive
				//
				// scoreOfSum = scoreOfEnemy1 + scoreOfEnemy2 + .....
				// =====================================================================================
				vector<Actor>::iterator iter = mScene->one->mArmy.begin();
				while( iter != mScene->one->mArmy.end() )
				{// for each enemy
					// distance
					float distance = fabs((float)(iter->gridPosition[0]+ - i)) + fabs((float)(iter->gridPosition[1] - j));
					float scoreOfThisEnemy = distance*weightDistance + iter->health*weightHealth;
					// add
					scoreOfThisPosition += scoreOfThisEnemy;
					// next
					iter ++;
				}
				// store this position's score 
				scores.push_back(scoreOfThisPosition);
			}
		}
	// do not to forget the current position
	// if it is safer to wait, then do not make movement
	float scoreOfCurrentPosition = 0;
	vector<Actor>::iterator iter = mScene->one->mArmy.begin();
	while( iter != mScene->one->mArmy.end() )
	{// for each enemy
		// distance
		float distance = fabs((float)(iter->gridPosition[0]+ - selectedActor->gridPosition[0]))
			+ fabs((float)(iter->gridPosition[1] - selectedActor->gridPosition[1]));
		float scoreOfThisEnemy = distance*weightDistance + iter->health*weightHealth;
		// add
		scoreOfCurrentPosition += scoreOfThisEnemy;
		// next
		iter ++;
	}

	// find position that has the highest score
	vector<int>::iterator iterScore = scores.begin();
	int highest = *iterScore;
	while(iterScore != scores.end())
	{
		if(*iterScore>highest)
			highest = *iterScore;
		iterScore ++;
	}
	// compare the hgitest score with score of current position
	if( scoreOfCurrentPosition >= highest )
	{// the safest position is current position
		// wait
		selectedActor->wait();
		ended = true;
		return;
	}
	else
	{
		// move to that position
		int index = 0;
		iterScore = scores.begin();
		while(iterScore != scores.end())
		{
			if(*iterScore == highest)
				break;
			iterScore ++;
			index ++;
		}
		int count = -1;
		for(int i = 0; i != mScene->mMap->rowX; i ++)
			for(int j = 0; j != mScene->mMap->rowY; j ++)
			{
				if( mScene->mMap->moveArea[i][j] == 0 )
					// take count to get the safest position
					count ++;
				if( count == index )
				{// this position is the safest
					// move to this position
					selectedActor->moveTo(i,j);

					attackEnemyInRange();
					selectedActor->wait();
					
					ended = true;
					return;
				}
				
			}
	}
}

Actor *SimpleAI::getTarget()
{
	float weightDistance = -10;
	float weightHealth = -20;

	int mX = selectedActor->gridPosition[0];
	int mY = selectedActor->gridPosition[1];
	vector<int> scores;
	vector<Actor>::iterator iter = mScene->one->mArmy.begin();
	while( iter != mScene->one->mArmy.end() )
	{
		float distance = fabs((float)(iter->gridPosition[0] - mX)) + fabs((float)(iter->gridPosition[1] - mY));
		
		int value = distance * weightDistance + iter->health * weightHealth;
		scores.push_back(value);
		iter ++;
	}

	vector<int>::iterator iterScore = scores.begin();
	int highest = *iterScore;
	while( iterScore != scores.end() )
	{
		if(*iterScore > highest )
			highest = *iterScore;
		iterScore ++;
	}
	iterScore = scores.begin();
	int index = 0;
	while( iterScore != scores.end() )
	{
		if((*iterScore) == highest )
			break;
		iterScore ++;
		index ++;
	}

	int count = 0;
	iter = mScene->one->mArmy.begin();
	for( int i = 0; i != index; i ++ )
	{
		if( count == index )
			break;
		iter ++;
		count ++;
	}

	Actor *target = &(*iter);
	return target;
}

void SimpleAI::kill()
{
	// get a target
	Actor * target = getTarget();
	
	attackEnemyInRange();
	if(selectedActor->attackChance <= 0)
	{
		selectedActor->wait();
		ended = true;
		return;
	}
	else
	{
		int moveX;
		int moveY;
		if(mScene->mMap->calculateMove(moveX, moveY, 
			selectedActor->gridPosition[0], selectedActor->gridPosition[1],
			target->gridPosition[0], target->gridPosition[1],
			selectedActor->SPEED))
		{
			selectedActor->moveTo(moveX,moveY);
		}
		else
		{// if can not reach the target
			// attack other units
			bool none = false;
			int size = mScene->one->mArmy.size();
			int count = 0;
			target = mScene->one->getRandomActor();
			while( !mScene->mMap->calculateMove(moveX, moveY, 
			selectedActor->gridPosition[0], selectedActor->gridPosition[1],
			target->gridPosition[0], target->gridPosition[1],
			selectedActor->SPEED) )
			{
				if( count >= size )
				{
					none = true;
					break;
				}
				count ++;
			}
			if( none == false )
				selectedActor->moveTo(moveX,moveY);
		}
		attackEnemyInRange();
		selectedActor->wait();
		ended = true;
		return;
	}
}





void SimpleAI::onTouchesBegan( CCSet *touches, CCEvent *event )
{
}
void SimpleAI::onTouchesMoved( CCSet *touches, CCEvent *event )
{
}
void SimpleAI::onTouchesEnded( CCSet *touches, CCEvent *event )
{
}
void SimpleAI::onUpdate( float dt )
{
	if(plan())// kill
		kill();
	else// escape
		escape();
	State::onUpdate( dt );
}
void SimpleAI::onMenu(CCObject* pSender )
{
}