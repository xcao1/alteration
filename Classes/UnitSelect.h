#ifndef __UNIT_SELECT_H__
#define __UNIT_SELECT_H__
#include "State.h"
class UnitSelect:public State
{
private:

public:
	UnitSelect( PlayingScene *scene );
	~UnitSelect();

	void onTouchesBegan( CCSet *touches, CCEvent *event );
	void onTouchesMoved( CCSet *touches, CCEvent *event );
	void onTouchesEnded( CCSet *touches, CCEvent *event );
	
	void onUpdate( float dt );
    void onMenu(CCObject* pSender );
};
#endif // !__UNIT_SELECT_H__
