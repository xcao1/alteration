#ifndef __BASE_STATE_MACHINE_H__
#define __BASE_STATE_MACHINE_H__

#include "cocos2d.h"
using namespace cocos2d;
class BaseStateMachine
{
public:
	BaseStateMachine(){}
	~BaseStateMachine(){}
	// switch state
	virtual void switchState() = 0;
	// touch interaction
	virtual void onTouchesBegan( CCSet *touches, CCEvent *event ) = 0;
	virtual void onTouchesMoved( CCSet *touches, CCEvent *event ) = 0;
	virtual void onTouchesEnded( CCSet *touches, CCEvent *event ) = 0;
	// update current state
	virtual void onUpdate( float dt ) = 0;
	// menu
	virtual void onMenu(CCObject* pSender ) = 0;
};

#endif // !__BASE_STATE_MACHINE_H__
