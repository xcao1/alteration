#include "DrawAdvancedArea.h"
#include "GridMap.h"
DrawAdvancedArea::DrawAdvancedArea( GridMap *map ):mMap(map)
{
	mColor.r = 0;
	mColor.g = 255;
	mColor.b = 0;
	mColor.a = 0.35;
}

void DrawAdvancedArea::draw()
{
	for( int x = 0; x != mMap->rowX; x ++ )
		for( int y = 0; y != mMap->rowY; y ++ )
		{
			if( mMap->moveArea[x][y] == 0 )
				ccDrawSolidRect( ccp(x*mMap->gridSize,y*mMap->gridSize),ccp((x+1)*mMap->gridSize,(y+1)*mMap->gridSize),mColor );
		}
			
}