#ifndef __DRAW_ADVANCED_AREA_H__
#define __DRAW_ADVANCED_AREA_H__

#include "cocos2d.h"
class GridMap;
using namespace cocos2d;
class DrawAdvancedArea:public CCNode
{
private:
	GridMap *mMap;
	ccColor4F mColor;
public:
	DrawAdvancedArea( GridMap *map );
	~DrawAdvancedArea()
	{
		mMap = 0;
	}

	void draw(void);
	void setColor( unsigned int r, unsigned int g, unsigned int b, unsigned int a )
	{
		mColor.r = r;
		mColor.g = g;
		mColor.b = b;
		mColor.a = a;
	}
	
};

#endif // !__DRAW_ADVANCED_AREA_H__
