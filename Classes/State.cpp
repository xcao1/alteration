#include "State.h"
#include "Player.h"
#include <cmath>
#include <string>
State::PLAYER State::mPlayer = PlayerOne;
int State::bigTurnsUsed = -1;
int State::smallTurnsUsed = -1;
State::State( PlayingScene *scene ):BaseState(),
	mScene(scene),
	moveCamera(false),prevY(0),beginY(0),
	mainMenuUp(false),checkUnits(false),resuming(false)
{
}
State::~State()
{
}
void State::updateCamera()
{
	bool update = false;
	float unitY = 0;
	vector<Actor>::iterator iter;
	if( mPlayer == PlayerOne )
	{
		if( Actor *target = mScene->one->getActiveActor() )
		{	
			update = true;
			unitY = target->gridPosition[1];
		}
	}
	else if( mPlayer == PlayerTwo )
	{
		if( Actor *target = mScene->two->getActiveActor() )
		{	
			update = true;
			unitY = target->gridPosition[1];
		}
	}
	if( update == true )
	{
		setCamera(unitY);
	}
}
void State::setCamera( int gridY )
{
	float gridOriginY = mScene->mInteractiveScenes->getPositionY();
	int currentGridPositionY = fabs( gridOriginY )/mScene->mMap->gridSize;
	float moveY = 0;
	float newY = 0;
	if( currentGridPositionY > gridY )
	{
		moveY = (currentGridPositionY - gridY)*mScene->mMap->gridSize;
		newY = gridOriginY + moveY + mScene->winSize.height/2;
			
	}
	else if( currentGridPositionY < gridY )
	{
		moveY = (gridY - currentGridPositionY)*mScene->mMap->gridSize;
		newY = gridOriginY - moveY + mScene->winSize.height/2;
	}
	if( currentGridPositionY != gridY )
	{
		if( newY > 0 )
			newY = 0;
		else if( newY < -1*(mScene->mMap->gridSize*mScene->mMap->rowY - mScene->winSize.height))
			newY = -1*(mScene->mMap->gridSize*mScene->mMap->rowY - mScene->winSize.height);
		mScene->mInteractiveScenes->setPositionY(newY);
	}
}
void State::onTouchesBegan( CCSet *touches, CCEvent *event )
{
	int number = touches->count();
	if( number < 2 )
	{
		// choose one of the touches to work with
		CCTouch *touch = ( CCTouch * )( touches->anyObject() );
		CCPoint location = touch->locationInView();
		location = CCDirector::sharedDirector()->convertToGL(location);
		prevY = location.y;
		beginY = location.y;
	}
}
void State::onTouchesMoved( CCSet *touches, CCEvent *event )
{
	CCTouch *touch = ( CCTouch * )( touches->anyObject() );
	CCPoint location = touch->locationInView();
	location = CCDirector::sharedDirector()->convertToGL(location);
	int number = touches->count();
	if( number < 2 )
	{
		float moveY = location.y - beginY;
		if( fabs(moveY) > 10 )
		{
			moveCamera = true;
			float disY = location.y - prevY;
			float sceneY = mScene->mInteractiveScenes->getPositionY();
			float newY = sceneY + disY;
			if( newY <= mScene->mMenu->menu->getContentSize().height && newY >= -1*(mScene->mMap->gridSize*mScene->mMap->rowY - mScene->winSize.height) )
				mScene->mInteractiveScenes->setPositionY( newY );
		}
		prevY = location.y;
	}
}
void State::onTouchesEnded( CCSet *touches, CCEvent *event )
{
	resuming = false;
	if( moveCamera == false )
	{
		CCTouch *touch = ( CCTouch * )( touches->anyObject() );
		CCPoint location = touch->locationInView();
		location = CCDirector::sharedDirector()->convertToGL(location);
		CCPoint pos = mScene->mMenu->menu->getPosition();
		CCSize size = mScene->mMenu->menu->getContentSize();
		CCRect rect = CCRectMake( pos.x - size.width/2, pos.y - size.height/2, size.width, size.height );
		pos = mScene->mMenu->checkUnitsData->getPosition();
		size = mScene->mMenu->checkUnitsData->getContentSize();
		CCRect rect1 = CCRectMake( pos.x - size.width/2, pos.y - size.height/2, size.width, size.height );
		pos = mScene->mMenu->resumeGame->getPosition();
		size = mScene->mMenu->resumeGame->getContentSize();
		CCRect rect2 = CCRectMake( pos.x - size.width/2, pos.y - size.height/2, size.width, size.height );
		/*pos = mScene->mMenu->passTurn->getPosition();
		size = mScene->mMenu->passTurn->getContentSize();
		CCRect rect3 = CCRectMake( pos.x - size.width/2, pos.y - size.height/2, size.width, size.height );*/
		pos = mScene->mMenu->exitGame->getPosition();
		size = mScene->mMenu->exitGame->getContentSize();
		CCRect rect4 = CCRectMake( pos.x - size.width/2, pos.y - size.height/2, size.width, size.height );
		if( mainMenuUp == false )
		{
			if( rect.containsPoint(location) )
			{
				mainMenuUp = true;
				mScene->mMenu->checkUnitsData->setVisible(true);
				mScene->mMenu->resumeGame->setVisible(true);
				/*mScene->mMenu->passTurn->setVisible(true);*/
				mScene->mMenu->exitGame->setVisible(true);
			}
			if( checkUnits == true )
			{
				CCPoint origin = mScene->mInteractiveScenes->getPosition();
				int x = fabs(location.x-origin.x)/mScene->mMap->gridSize;
				int y = fabs(location.y-origin.y)/mScene->mMap->gridSize;
				
				bool selected = false;
				Actor *target;
				if( target = mScene->one->getActorAtPosition(x,y) )
					selected = true;
				else
				{
					if( target = mScene->two->getActorAtPosition(x,y) )
						selected = true;
				}
				if( selected == true )
					target->showData();
			}
		}
		else
		{// main menu is up
			if( rect.containsPoint(location) )
			{// close menu
				mainMenuUp = false;
				mScene->mMenu->checkUnitsData->setVisible(false);
				mScene->mMenu->resumeGame->setVisible(false);
				/*mScene->mMenu->passTurn->setVisible(false);*/
				mScene->mMenu->exitGame->setVisible(false);
			}
			else if( rect1.containsPoint(location) )
			{// check units data
				mainMenuUp = false;
				mScene->mMenu->checkUnitsData->setVisible(false);
				mScene->mMenu->resumeGame->setVisible(false);
				/*mScene->mMenu->passTurn->setVisible(false);*/
				mScene->mMenu->exitGame->setVisible(false);

				checkUnits = true;
			}
			else if( rect2.containsPoint(location) )
			{// resume game
				mainMenuUp = false;
				mScene->mMenu->checkUnitsData->setVisible(false);
				mScene->mMenu->resumeGame->setVisible(false);
				/*mScene->mMenu->passTurn->setVisible(false);*/
				mScene->mMenu->exitGame->setVisible(false);

				resuming = true;
				checkUnits = false;
				// hide information
				mScene->mMenu->maxHealth->setVisible(false);
				mScene->mMenu->health->setVisible(false);
				mScene->mMenu->ATK->setVisible(false);
				mScene->mMenu->SPEED->setVisible(false);
				mScene->mMenu->RANGE->setVisible(false);
			}
			/*else if( rect3.containsPoint( location) )
			{
				mScene->mMenu->checkUnitsData->setVisible(false);
				mScene->mMenu->resumeGame->setVisible(false);
				mScene->mMenu->passTurn->setVisible(false);
				mScene->mMenu->exitGame->setVisible(false);

				if( mPlayer == PlayerOne )
					mScene->one->deactivateAll();
				else
					mScene->two->deactivateAll();
			}*/
			else if( rect4.containsPoint(location) )
			{// exit game
				mScene->gameOVer();
			}
		}	
	}
}
void State::onUpdate( float dt )
{
	// army 1
	mScene->one->onUpdate();
	// army 2
	mScene->two->onUpdate();
}
void State::onMenu(CCObject* pSender )
{
}