#include "Player.h"
#include <cstdlib>
#include "PlayingScene.h"
#include "GridMap.h"
Player::Player( PlayingScene *scene, int score, bool isAI, bool side ):mScene(scene),
	mScore(score),mIsAI(isAI),mSide(side)
{
}
Player::~Player()
{
}

void Player::add( int x, int y )
{
	mArmy.push_back(Actor(mScene,x,y,mSide));
}
Actor *Player::getSelectedActor()
{
	vector<Actor>::iterator iter;
	iter = mArmy.begin();
	while( iter != mArmy.end() )
	{
		if( iter->selected == true )
		{
			Actor *selectedActor = &(*iter);
			return selectedActor;
		}
		iter ++;
	}
	return 0;
}
Actor *Player::getActorAtPosition( int x, int y )
{
	vector<Actor>::iterator iter;
	iter = mArmy.begin();
	while( iter != mArmy.end() )
	{
		if( iter->gridPosition[0] == x && iter->gridPosition[1] == y )
		{
			Actor *selectedActor = &(*iter);
			return selectedActor;
		}
		iter ++;
	}
	return 0;
}
Actor *Player::getActiveActor()
{
	vector<Actor>::iterator iter;
	
	iter = mArmy.begin();
	while( iter != mArmy.end() )
	{
		if( iter->isActive() )
		{
			Actor *selectedActor = &(*iter);
			return selectedActor;
		}
		iter ++;
	}
	return 0;
}
Actor *Player::getInDangerActor()
{
	int lowestHealth;
	vector<Actor>::iterator iter;
	
	iter = mArmy.begin();
	lowestHealth = iter->health;
	while( iter != mArmy.end() )
	{
		if( iter->health < lowestHealth )
			lowestHealth = iter->health;
		iter ++;
	}
	
	iter = mArmy.begin();
	while( iter != mArmy.end() )
	{
		if( iter->health = lowestHealth )
		{
			Actor *selectedActor = &(*iter);
			return selectedActor;
		}
		iter ++;
	}
}
Actor *Player::getRandomActor()
{
	int num = rand()%mArmy.size();
	vector<Actor>::iterator iter;
	iter = mArmy.begin();
	int index = 0;
	while( iter != mArmy.end() )
	{
		if(index == num)
		{
			Actor *selectedActor = &(*iter);
			return selectedActor;
		}
		index ++;
		iter ++;
	}
}
bool Player::onAttack(Actor *attacker, int x, int y )
{
	bool gotAttack = false;
	vector<Actor>::iterator iter;
	iter = mArmy.begin();
	while( iter != mArmy.end() )
	{
		if( iter->gridPosition[0] == x && iter->gridPosition[1] == y )
		{
			gotAttack = true;
			break;
		}
		iter ++;
	}
	if( gotAttack )
	{
		iter->health -= attacker->ATK;
		if( !iter->isAlive() )
		{
			// update score
			mScore -= 50;
			// clear the position on the map
			mScene->mMap->gridMap[x][y] = 0;
			// update sprite image
			iter->hideSprites();
			iter->removeSprites();
			mArmy.erase( iter );
		}
	}
	return gotAttack;
}

bool Player::isEmpty()
{
	if(mArmy.empty())
		return true;
	else 
		return false;
}
void Player::deactivateAll()
{
	vector<Actor>::iterator iter = mArmy.begin();
	while( iter != mArmy.end() )
	{
		iter->wait();
		iter ++;
	}
}
void Player::refresh()
{
	vector<Actor>::iterator iter = mArmy.begin();
	while( iter != mArmy.end() )
	{
		iter->refresh();
		iter ++;
	}
}
void Player::recover()
{
	vector<Actor>::iterator iter = mArmy.begin();
	while( iter != mArmy.end() )
	{
		iter->recoverHealth();
		iter ++;
	}
}
void Player::levelUp()
{
	vector<Actor>::iterator iter = mArmy.begin();
	while( iter != mArmy.end() )
	{
		iter->levelUp();
		iter ++;
	}
}
bool Player::anyActive()
{
	bool any = false;
	vector<Actor>::iterator iter;
	
	iter = mArmy.begin();
	while( iter != mArmy.end() )
	{
		if( iter->isActive() )
			any = true;
		iter ++;
	}
	return any;
}

void Player::onUpdate()
{
	vector<Actor>::iterator iter = mArmy.begin();
	while( iter != mArmy.end() )
	{
		iter->onUpdate();
		iter ++;
	}
}
