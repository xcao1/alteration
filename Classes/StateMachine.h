#ifndef __STATE_MACHINE_H__
#define __STATE_MACHINE_H__
#include "cocos2d.h"
#include "BaseStateMachine.h"
using namespace cocos2d;
// every time the sate machine only runs on one state
class State;
class PlayingScene;
enum MAIN_LOOP
{
	// ======================
	// SYSTEM
	TURNCOUNT,
	RECOVER, 
	// ======================
	// PLAYER
	SUMMON, 
	UNITSELECT,
	ACT,//MOVE, ATTACK, WAIT,
	AI
	//END
};
class StateMachine:public BaseStateMachine
{
private:
	PlayingScene *mScene;
	// pointer to current state
	State *mCurrentState;
public:
	StateMachine( PlayingScene *scene );
	~StateMachine();
	// switch state
	void switchState();
	// touch interaction
	void onTouchesBegan( CCSet *touches, CCEvent *event );
	void onTouchesMoved( CCSet *touches, CCEvent *event );
	void onTouchesEnded( CCSet *touches, CCEvent *event );
	// update current state
	void onUpdate( float dt );
	// menu
	void onMenu(CCObject* pSender );
};





#endif