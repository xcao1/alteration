#ifndef __STATE_NAME_H__
#define __STATE_NAME_H__

enum MAIN_LOOP
{
	// ======================
	// SYSTEM
	TURNCOUNT,
	RECOVER, 
	// ======================
	// PLAYER
	SUMMON, 
	UNITSELECT,
	ACT,//MOVE, ATTACK, WAIT,
	//END
};
#endif // !__STATE_NAME_H__
