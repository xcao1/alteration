#ifndef __STATE_H__
#define __STATE_H__
#include "cocos2d.h"
#include "StateMachine.h"
#include "PlayingScene.h"
#include "Actor.h"
#include "GridMap.h"
#include "Menu.h"
#include "BaseState.h"
using namespace cocos2d;
class PlayingScene;
class State:public BaseState
{
protected:
	bool mainMenuUp;
	bool checkUnits;
	bool resuming;
	PlayingScene *mScene;
	float prevY;
	float beginY;
	bool moveCamera;

	enum PLAYER
	{
		PlayerOne,
		PlayerTwo
	};
	static PLAYER mPlayer;
	static int bigTurnsUsed;
	static int smallTurnsUsed;
public:
	MAIN_LOOP nextName;
	State( PlayingScene *scene );
	~State();
	void onTouchesBegan( CCSet *touches, CCEvent *event );
	void onTouchesMoved( CCSet *touches, CCEvent *event );
	void onTouchesEnded( CCSet *touches, CCEvent *event );
	void onUpdate( float dt );
    void onMenu(CCObject* pSender );
	
	void updateCamera();
	void setCamera( int gridY );
};
#endif // !__STATE_H__