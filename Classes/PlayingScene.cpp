#include "PlayingScene.h"
#include "StateMachine.h"
#include "GridMap.h"
#include "Menu.h"
#include "Player.h"
#include "MenuScene.h"
#include <stdio.h>
#include <string>
PlayingScene::PlayingScene():mMap(NULL),mMenu(NULL),
	layerBackground(0),layerElements(1),layerInformation(2),layerMenu(3),
	mStateMachine(NULL),
	vsAI(false)
{	
}
PlayingScene::~PlayingScene()
{
	delete mStateMachine;
	mStateMachine = NULL;
}
CCScene* PlayingScene::scene()
{
	CCScene * scene = NULL;
    // 'scene' is an autorelease object
    scene = CCScene::create();
    if(scene)
	{
		// 'layer' is an autorelease object
		PlayingScene *layer = PlayingScene::create();
		if( layer)
			scene->addChild(layer);
	}
    return scene;
}
void PlayingScene::ccTouchesBegan(CCSet *touches, CCEvent *event)
{
	mStateMachine->onTouchesBegan( touches, event );
}
void PlayingScene::ccTouchesMoved(CCSet *touches, CCEvent *event)
{
	mStateMachine->onTouchesMoved( touches, event );
}
void PlayingScene::ccTouchesEnded(CCSet *touches, CCEvent * event)
{
	mStateMachine->onTouchesEnded( touches, event );
}
// on "init" you need to initialize your instance
bool PlayingScene::init()
{
	// ============================================================
	// the flag used to return
    bool bRet = false;
	// super init first
    if(! CCLayer::init())
		return bRet;
    // =======================================================
	mInteractiveScenes = new CCNode;
	addChild( mInteractiveScenes );

	winSize = CCDirector::sharedDirector()->getWinSize();
	mMap = new GridMap( 12, 12, winSize.width/12, this );
	mStateMachine = new StateMachine(this);



	one = new Player( this, 1000, false, true );
	if( !vsAI )
		two = new Player( this, 1000, false, false );
	else
		two = new Player( this, 1000, true, false );
	// army 1
	//one->add(0,0);
	one->add(1,4);
	//one->add(2,0);
	one->add(3,2);
	//one->add(4,0);
	one->add(5,2);
	one->add(6,2);
	//one->add(7,0);
	one->add(8,2);
	//one->add(9,0);
	one->add(10,4);
	//one->add(11,0);
	
	// army 2
	//two->add(0,11);
	two->add(1,7);
	//two->add(2,11);
	two->add(3,9);
	//two->add(4,11);
	two->add(5,9);
	two->add(6,9);
	//two->add(7,11);
	two->add(8,9);
	//two->add(9,11);
	two->add(10,7);
	//two->add(11,11);
	

	mMenu = new Menu( this );
	// ===============
	setTouchEnabled( true );
	// update the game 
	schedule( schedule_selector( PlayingScene::update ));
	
	bRet = true;
	return bRet;
}
// cpp with cocos2d-x
void PlayingScene::update( float dt )
{
	mStateMachine->onUpdate( dt );
} 
void PlayingScene::menuCloseCallback(CCObject* pSender)
{
}



void PlayingScene::gameOVer()
{
	CCScene *scene = CCScene::create();
	if(scene)
	{
		// 'layer' is an autorelease object
		MenuScene *layer = new MenuScene;
		if( layer)
		{
			if(layer->init())
			{
				layer->setState(false);
				scene->addChild(layer);
				// set the score to print
				char value1[20];
				char value2[20];
				std::string messageString;
				sprintf(value1,"%d",one->mScore);
				sprintf(value2,"%d",two->mScore);
				messageString = "PlayerOne's Score : "+ std::string(value1)
					+"\nPlayerTwo's Score : " + std::string(value2);
				if( one->mScore > two->mScore )
					messageString += "\nPlayerOne Win !";
				else if( one->mScore < two->mScore )
					messageString += "\nPlayerTwo Win !";
				else
					messageString += "\nDraw !";
				layer->printScore->setString(messageString.c_str());
				// replace scene
				CCDirector::sharedDirector()->replaceScene(scene);
			}
		}
	}
}