#ifndef __GRID_MAP_H__
#define __GRID_MAP_H__
#include <vector>
using namespace std;
class PlayingScene;
class DrawGrids;
class DrawArea;
class DrawAdvancedArea;
class Grid
{
public:
	int x;
	int y;
	Grid(int inX, int inY):x(inX),y(inY){}
	~Grid(){}
};
class GridNode : public Grid
{
public:
	int gVal;
	int fVal;
	class GridNode *next;
	class GridNode *parent;
	class GridNode *right;
	class GridNode *down;
	class GridNode *left;
	class GridNode *up;
	GridNode(int inX, int inY, class GridNode *p ):Grid(inX,inY),
		gVal(0),fVal(0),
		next(0),parent(p),
		right(0),down(0),left(0),up(0){}
	~GridNode()
	{
		delete next;
		next = 0;
		delete parent;
		parent = 0;
		delete right;
		right = 0;
		delete down;
		down = 0;
		delete left;
		left = 0;
		delete up;
		up = 0;
	}
};



class GridMap
{
private:
	GridNode *mHead;
public:
	float gridSize;
	int rowX;
	int rowY;
	int **gridMap;
	int **moveArea;
	
	DrawArea *mArea;
	DrawGrids *mGrids;
	DrawAdvancedArea *mAdvancedArea;
	GridMap( int x, int y, float size, PlayingScene *scene );
	~GridMap();

	void search(GridNode *p, int steps, int range);
	// calculate move area and store it into the 2d array
	void calculateMoveArea(int x, int y, int range);

	bool isNeighbor(int x1, int y1 ,int x2, int y2);
	void removeFromSet( vector<GridNode*> &set, GridNode *p );
	bool isInSet( const vector<GridNode*> &set, int x, int y );
	GridNode *lowestInSet( const vector<GridNode*> &set);
	GridNode * AStar( vector<GridNode*> &open, vector<GridNode*> &close, GridNode *p,int startX, int startY, int goalX, int goalY);
	
	bool calculateMove(int &moveX, int &moveY,int startX, int startY, int goalX, int goalY, int range);
};
#endif // !__GRID_MAP_H__
