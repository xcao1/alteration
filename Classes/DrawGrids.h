#ifndef __DRAW_GRIDS_H__
#define __DRAW_GRIDS_H__

#include "cocos2d.h"

using namespace cocos2d;
class DrawGrids:public CCNode
{
private:
	float gridSize;
	float rowX;
	float rowY;
public:
	DrawGrids();
	DrawGrids( int rX, int rY, float size );
	~DrawGrids();

	void draw(void);
	void setRows( int rX, int rY );
	void setGridSize( float size );
};
#endif // !__DRAW_GRIDS_H__
