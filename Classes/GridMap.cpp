#include "GridMap.h"
#include "PlayingScene.h"
#include "DrawGrids.h"
#include "DrawArea.h"
#include "DrawAdvancedArea.h"
#include <cmath>
#include <stdexcept>
#include <vector>
using namespace std;
GridMap::GridMap( int x, int y, float size, PlayingScene *scene ):rowX(x),rowY(y),gridSize(size),
	mArea(0),mGrids(0),mAdvancedArea(0),
	mHead(0)
{
	
		gridMap = new int *[rowX];
		for( int i = 0; i != rowX; i ++ )
			gridMap[i] = new int [rowY];
		for( int i = 0; i != rowX; i ++ )
			for( int j = 0; j != rowY; j ++ )
				gridMap[i][j] = 0;
		moveArea = new int *[rowX];
		for( int i = 0; i != rowX; i ++ )
			moveArea[i] = new int [rowY];
		for( int i = 0; i != rowX; i ++ )
			for( int j = 0; j != rowY; j ++ )
				moveArea[i][j] = 1;
		
		// area
		mArea = new DrawArea( gridSize );
		mArea->setVisible(false);
		scene->mInteractiveScenes->addChild( mArea, scene->layerBackground );
		// grids
		mGrids = new DrawGrids( rowX, rowY, gridSize );
		scene->mInteractiveScenes->addChild( mGrids, scene->layerBackground );

		// move area
		mAdvancedArea = new DrawAdvancedArea( this );
		mAdvancedArea->setVisible(false);
		scene->mInteractiveScenes->addChild( mAdvancedArea, scene->layerBackground );
}

void GridMap::search(GridNode *p, int steps, int range )
{
	if(steps < range)
	{
		if( p->x+1 < rowX )
		{
			if(p->right == 0 && gridMap[p->x+1][p->y] == 0 )
			{
				moveArea[p->x+1][p->y] = 0;
				p->right = new GridNode(p->x+1,p->y,p);
				p->right->left = p;
			}
		}
		if( p->y-1 >= 0 )
		{
			if(p->down == 0 && gridMap[p->x][p->y-1] == 0 )
			{
				moveArea[p->x][p->y-1] = 0;
				p->down = new GridNode(p->x,p->y-1,p);
				p->down->up = p;
			}
		}
		if( p->x-1 >= 0 )
		{
			if(p->left == 0 && gridMap[p->x-1][p->y] == 0 )
			{
				moveArea[p->x-1][p->y] = 0;
				p->left = new GridNode(p->x-1,p->y,p);
				p->left->right = p;
			}
		}
		if( p->y+1 < rowY )
		{
			if(p->up == 0 && gridMap[p->x][p->y+1] == 0 )
			{
				moveArea[p->x][p->y+1] = 0;
				p->up = new GridNode(p->x,p->y+1,p);
				p->up->down = p;
			}
		}
		
		steps ++;

		if(p->right && p->right != p->parent)
			search(p->right,steps,range);
		if(p->down && p->down != p->parent)
			search(p->down,steps,range);
		if(p->left && p->left != p->parent)
			search(p->left,steps,range);
		if(p->up && p->up != p->parent)
			search(p->up,steps,range);
	}
}

void GridMap::calculateMoveArea(int x, int y, int range)
{
	for( int i = 0; i != rowX; i ++ )
		for( int j = 0; j != rowY; j ++ )
			moveArea[i][j] = 1;
	if( x >= 0 && x < rowX && y >= 0 && y < rowY && range > 0 )
	{
		GridNode *pNode = new GridNode(x,y,0);
		mHead = pNode;
		search(pNode,0,range);
	}
}
bool GridMap::isNeighbor(int x1, int y1 ,int x2, int y2)
{
	float distance = fabs((float)(x1-x2))+fabs((float)(y1-y2));
	if( distance == 1 )
		return true;
	else
		return false;
}
bool GridMap::isInSet( const vector<GridNode*> &set, int x, int y )
{
	vector<GridNode*>::const_iterator iter = set.begin();
	while( iter != set.end() )
	{
		if((*iter)->x == x && (*iter)->y == y)
			return true;
		iter ++;
	}
	return false;
}
void GridMap::removeFromSet( vector<GridNode*> &set, GridNode *p )
{
	vector<GridNode*>::iterator iter = set.begin();
	while( iter != set.end() )
	{
		if((*iter)->x == p->x && (*iter)->y == p->y)
		{
			set.erase(iter);
			return;
		}
		iter ++;
	}
}
GridNode *GridMap::lowestInSet( const vector<GridNode*> &set)
{
	if(set.empty())
		return 0;
	else
	{
		vector<GridNode*>::const_iterator iter = set.begin();
		float lowest = (*iter)->fVal;
		int index =0;
		int num = 0;
		while( iter != set.end() )
		{
			if((*iter)->fVal < lowest)
			{
				lowest = (*iter)->fVal;
				num = index;
			}
			iter ++;
			index ++;
		}

		return set[num];
	}
}
GridNode *GridMap::AStar( vector<GridNode*> &open, vector<GridNode*> &close, GridNode *p, int startX, int startY, int goalX, int goalY )
{
	if(isNeighbor(p->x,p->y,goalX,goalY))
		return p;
	else
	{// have not reached the goal
		// continue
		if( p->x+1 < rowX )
		{
			if( gridMap[p->x+1][p->y] == 0 )
			{
				if( !isInSet( open, p->x+1, p->y ) && !isInSet( close, p->x+1, p->y ) )
				{
					p->right = new GridNode(p->x+1,p->y,p);
					p->right->left = p;

					// evaluate
					p->right->gVal = p->gVal +  1;
					p->right->fVal = p->right->gVal + fabs((float)(goalX - p->right->x)) + fabs((float)(goalY - p->right->y));

					open.push_back( p->right );
				}
			}
		}
		if( p->y-1 >= 0 )
		{
			if( gridMap[p->x][p->y-1] == 0 )
			{
				if( !isInSet( open, p->x,p->y-1 ) && !isInSet( close, p->x,p->y-1 ) )
				{
					p->down = new GridNode(p->x,p->y-1,p);
					p->down->up = p;

					// evaluate
					p->down->gVal = p->gVal +  1;
					p->down->fVal = p->down->gVal + fabs((float)(goalX - p->down->x)) + fabs((float)(goalY - p->down->y));

					open.push_back(p->down);
				}
			}
		}
		if( p->x-1 >= 0 )
		{
			if( gridMap[p->x-1][p->y] == 0 )
			{
				if( !isInSet( open, p->x-1,p->y ) && !isInSet( close, p->x-1,p->y ) )
				{
					p->left = new GridNode(p->x-1,p->y,p);
					p->left->right = p;

					// evaluate
					p->left->gVal = p->gVal +  1;
					p->left->fVal = p->left->gVal + fabs((float)(goalX - p->left->x)) + fabs((float)(goalY - p->left->y));

					open.push_back(p->left);
				}
			}
		}
		if( p->y+1 < rowY )
		{
			if( gridMap[p->x][p->y+1] == 0 )
			{
				if( !isInSet( open, p->x,p->y+1 ) && !isInSet( close, p->x,p->y+1 ) )
				{
					p->up = new GridNode(p->x,p->y+1,p);
					p->up->down = p;

					// evaluate
					p->up->gVal = p->gVal +  1;
					p->up->fVal = p->up->gVal + fabs((float)(goalX - p->up->x)) + fabs((float)(goalY - p->up->y));

					open.push_back(p->up);
				}
			}
		}
		// remove current point from openset
		removeFromSet( open, p );
		// add current point to closeset
		close.push_back( p );

		// if openset is empty
		// return failure
		if(open.empty())
			return 0;
		else
		{
			// chose a node that has the lowest fVal
			GridNode * lowest = lowestInSet(open);
			AStar(open,close,lowest,startX,startY,goalX,goalY);
		}
	}
}
bool GridMap::calculateMove(int &moveX, int &moveY,int startX, int startY, int goalX, int goalY, int range)
{
	if( startX >= 0 && startX < rowX && startY >= 0 && startY < rowY 
		&& goalX >= 0 && goalX < rowX && goalY >= 0 && goalY < rowY
		&& range > 0 )
	{
		// open set 
		vector<GridNode*> openSet;
		// close set
		vector<GridNode*> closeSet;
		
		
		// start node
		GridNode *pNode = new GridNode(startX,startY,0);
		mHead = pNode;
		pNode->gVal = 0;
		pNode->fVal = fabs((float)(goalX-startX))+fabs((float)(goalY-startY));
		openSet.push_back(pNode);

		if( pNode = AStar( openSet, closeSet, pNode, startX, startY, goalX, goalY ))
		{// construct path
			if( fabs((float)(pNode->x - mHead->x)) + fabs((float)(pNode->y-mHead->y)) <= range )
			{
				moveX = pNode->x;
				moveY = pNode->y;
			}
			else
			{
				while(pNode->parent != 0)
				{
					pNode->parent->next = pNode;
					pNode = pNode->parent;
				}
				int steps = range;
				while( pNode->next->next != 0 )
				{
					pNode = pNode->next;
					steps --;
					if(steps == 0)
						break;
				}
				moveX = pNode->x;
				moveY = pNode->y;
			}
			
			return true;
		}
		else
			return false;
	}
	else
		return false;
}