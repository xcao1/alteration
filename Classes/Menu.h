#ifndef __MENU_H__
#define __MENU_H__
#include "cocos2d.h"
using namespace cocos2d;
class PlayingScene;
class Menu
{
public:
	CCSprite *menuMove;
	CCSprite *menuAttack;
	CCSprite *menuWait;
	CCSprite *menu;
	CCSprite *checkUnitsData;
	CCSprite *resumeGame;
	CCSprite *exitGame;
	//CCSprite *passTurn;

	CCLabelTTF* maxHealth;
	CCLabelTTF* health;
	CCLabelTTF* ATK;
	CCLabelTTF* SPEED;
	CCLabelTTF* RANGE;


	Menu(PlayingScene *scene);
	~Menu();
};
#endif // !__MENU_H__
