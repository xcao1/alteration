#include "Act.h"
#include "MenuScene.h"
#include "DrawArea.h"
#include "DrawAdvancedArea.h"
#include <cmath>
#include "Player.h"
Act::Act(PlayingScene *scene):State(scene),menuUp(true),mAction(None)
{
	ended = false;
	nextName = UNITSELECT;

	// ==========================
	// store the selected units into a member pointer
	if( mPlayer == PlayerOne )
		selectedActor = mScene->one->getSelectedActor();
	else if( mPlayer == PlayerTwo )
		selectedActor = mScene->two->getSelectedActor();
}
Act::~Act()
{
}

void Act::onTouchesBegan( CCSet *touches, CCEvent *event )
{
	moveCamera = false;
	if( menuUp == false )
		State::onTouchesBegan( touches, event );
}
void Act::onTouchesMoved( CCSet *touches, CCEvent *event )
{
	if( menuUp == false )
		State::onTouchesMoved( touches, event );// if camera moved
}
void Act::onTouchesEnded( CCSet *touches, CCEvent *event )
{
	State::onTouchesEnded(touches,event);
	if(  !( checkUnits || mainMenuUp || resuming ) )
	{
		CCTouch *touch = ( CCTouch * )( touches->anyObject() );
		CCPoint location = touch->locationInView();
		location = CCDirector::sharedDirector()->convertToGL(location);
		CCPoint origin = mScene->mInteractiveScenes->getPosition();
		if( menuUp == true )
		{// select a action
			CCPoint pos = mScene->mMenu->menuMove->getPosition();
			CCSize size = mScene->mMenu->menuMove->getContentSize();
			CCRect rectM = CCRectMake( pos.x - size.width/2, pos.y - size.height/2, size.width, size.height );
			pos = mScene->mMenu->menuAttack->getPosition();
			size = mScene->mMenu->menuAttack->getContentSize();
			CCRect rectA = CCRectMake( pos.x - size.width/2, pos.y - size.height/2, size.width, size.height );
			pos = mScene->mMenu->menuWait->getPosition();
			size = mScene->mMenu->menuWait->getContentSize();
			CCRect rectW = CCRectMake( pos.x - size.width/2, pos.y - size.height/2, size.width, size.height );
			if( rectM.containsPoint(location) )
			{// move
				if( selectedActor->moveChance > 0 )
				{
					mAction = Move;
					menuUp = false;
					// show area
					mScene->mMap->calculateMoveArea(selectedActor->gridPosition[0],selectedActor->gridPosition[1],selectedActor->SPEED);
					mScene->mMap->mAdvancedArea->setVisible(true);
				}	
			}
			else if ( rectA.containsPoint(location) )
			{// attack
				if( selectedActor->attackChance > 0 )
				{
					mAction = Attack;
					menuUp = false;
					// show area
					mScene->mMap->mArea->set(selectedActor->gridPosition[0],selectedActor->gridPosition[1],selectedActor->RANGE,1);
					mScene->mMap->mArea->setVisible(true);
				}	
			}
			else if ( rectW.containsPoint(location) )
			{// wait
				mAction = Wait;
				menuUp = false;
			}
		}
		else
		{// if action selected
			if( moveCamera == false )
			{// if not moving the camera, act
				int x = fabs(location.x-origin.x)/mScene->mMap->gridSize;
				int y = fabs(location.y-origin.y)/mScene->mMap->gridSize;
				if( mAction == Move )
				{
					if( mScene->mMap->moveArea[x][y] == 1 )
					{
						mAction = None;
						menuUp = true;
						mScene->mMap->mAdvancedArea->setVisible(false);
					}
					else
					{// continue this action
						if( x >= 0 && x < mScene->mMap->rowX && y >= 0 && y < mScene->mMap->rowY )
						{
							// if the move is legal, according to the speed of the unit
							// if there is no units, able to move to there
							mScene->mMap->gridMap[selectedActor->gridPosition[0]][selectedActor->gridPosition[1]] = 0;
							selectedActor->gridPosition[0] = x;
							selectedActor->gridPosition[1] = y;
							if( mPlayer == PlayerOne )
								mScene->mMap->gridMap[x][y] = 1;
							else if( mPlayer == PlayerTwo )
								mScene->mMap->gridMap[x][y] = -1;

							// movechace - 1
							selectedActor->moveChance --;
							if( !selectedActor->isActive() )
							{
								selectedActor->selected = false;
								mAction = Wait;
								ended = true;
							}
							else
							{
								mAction = None;
								menuUp = true;
							}
							// hide area
							mScene->mMap->mAdvancedArea->setVisible(false);
						}
					}
				}
				else if( mAction == Attack )
				{
					int distanceX = x - selectedActor->gridPosition[0]; distanceX = fabs((float)distanceX);
					int distanceY = y - selectedActor->gridPosition[1]; distanceY = fabs((float)distanceY);
					if( (distanceX + distanceY) > selectedActor->RANGE )
					{
						mAction = None;
						menuUp = true;
						mScene->mMap->mArea->setVisible(false);
					}
					else
					{// continue this action
						// if the target is in the range of the unit's attack
						if( mPlayer == PlayerTwo )
						{// attacking player1's units
							if( !mScene->one->getActorAtPosition(x,y) )
							{
								mAction = None;
								menuUp = true;
								mScene->mMap->mArea->setVisible(false);
							}
							else
							{
								mScene->one->onAttack(selectedActor,x,y);
								// check if win
								if( mScene->one->isEmpty() )
								{// win
									mScene->gameOVer();
								}
								// hide area
								mScene->mMap->mArea->setVisible(false);
								// attack chance - 1
								selectedActor->attackChance --;
								if( !selectedActor->isActive())
								{
									selectedActor->selected = false;
									mAction = Wait;
									ended = true;
								}
								else
								{
									mAction = None;
									menuUp = true;
								}
							}

						}
						else if( mPlayer == PlayerOne )
						{// attacking player2's units
							if( !mScene->two->getActorAtPosition(x,y) )
							{
								mAction = None;
								menuUp = true;
								mScene->mMap->mArea->setVisible(false);
							}
							else
							{
								mScene->two->onAttack(selectedActor,x,y);
								// check if win
								if( mScene->two->isEmpty() )
								{// win
									mScene->gameOVer();
								}
								mScene->mMap->mArea->setVisible(false);
								// attack chance - 1
								selectedActor->attackChance --;
								if( !selectedActor->isActive())
								{
									selectedActor->selected = false;
									mAction = Wait;
									ended = true;
								}
								else
								{
									mAction = None;
									menuUp = true;
								}
							}
						}
					}
				}
			}
		}
	}
}
void Act::onUpdate( float dt )
{
	if( menuUp == true )
	{
		mScene->mMenu->menuMove->setVisible(true);
		mScene->mMenu->menuAttack->setVisible(true);
		mScene->mMenu->menuWait->setVisible(true);
	}
	else
	{
		mScene->mMenu->menuMove->setVisible(false);
		mScene->mMenu->menuAttack->setVisible(false);
		mScene->mMenu->menuWait->setVisible(false);
	}
	if( mAction == Wait )
	{
		selectedActor->attackChance = 0;
		selectedActor->moveChance = 0;
		selectedActor->selected = false;
		ended = true;
	}
	State::onUpdate( dt );
}
void Act::onMenu(CCObject* pSender )
{
}