#include "TurnCount.h"
#include "MenuScene.h"
#include "Player.h"
#include <ctime>
#include <cstdlib>
TurnCount::TurnCount( PlayingScene *scene ):State( scene )
{
	bigTurnsUsed ++;
	ended = false;
	nextName = UNITSELECT;
}
TurnCount::~TurnCount()
{
}
void TurnCount::onTouchesBegan( CCSet *touches, CCEvent *event )
{
}
void TurnCount::onTouchesMoved( CCSet *touches, CCEvent *event )
{
}
void TurnCount::onTouchesEnded( CCSet *touches, CCEvent *event )
{
}
void TurnCount::onUpdate( float dt )
{
	if(bigTurnsUsed%2 == 0)
	{
		// level up
		mScene->one->levelUp();
		mScene->two->levelUp();
	}
	
	// reset
	mScene->one->refresh();
	mScene->two->refresh();
	// recover health
	mScene->one->recover();
	mScene->two->recover();

	ended = true;
}
void TurnCount::onMenu(CCObject* pSender )
{
}