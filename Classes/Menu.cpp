#include "Menu.h"
#include "PlayingScene.h"
Menu::Menu(PlayingScene *scene):
	menuMove(NULL),menuAttack(NULL),menuWait(NULL),
	menu(NULL),checkUnitsData(NULL),resumeGame(NULL),exitGame(NULL),/*passTurn(NULL),*/
	maxHealth(NULL),health(NULL),ATK(NULL),SPEED(NULL),RANGE(NULL)
{
	menuAttack =  CCSprite::create( "attack0.png" );
	menuAttack->setPosition(ccp(scene->winSize.width/2, scene->winSize.height/2 + menuAttack->getContentSize().height/2));
	menuAttack->setPositionY(menuAttack->getPositionY() + menuAttack->getContentSize().height);
	menuAttack->setVisible(false);
	scene->addChild( menuAttack, scene->layerMenu );

	menuMove =  CCSprite::create( "move0.png" );
	menuMove->setPosition(ccp(scene->winSize.width/2, scene->winSize.height/2 ));
	menuMove->setVisible(false);
	scene->addChild( menuMove, scene->layerMenu );

	menuWait =  CCSprite::create( "wait0.png" );
	menuWait->setPosition(ccp(scene->winSize.width/2, scene->winSize.height/2 - menuWait->getContentSize().height/2));
	menuWait->setPositionY(menuWait->getPositionY() - menuWait->getContentSize().height);
	menuWait->setVisible(false);
	scene->addChild( menuWait, scene->layerMenu );

	// menu
	menu = CCSprite::create( "menu.png" );
	menu->setPosition(ccp(scene->winSize.width - menu->getContentSize().width/2, menu->getContentSize().height/2));
	menu->setVisible(true);
	scene->addChild( menu, scene->layerMenu);

	checkUnitsData = CCSprite::create( "checkunitsdata.png" );
	checkUnitsData->setPosition(ccp(scene->winSize.width/2,scene->winSize.height
		-checkUnitsData->getContentSize().height));
	checkUnitsData->setVisible(false);
	scene->addChild( checkUnitsData, scene->layerMenu );

	resumeGame = CCSprite::create( "resumegame.png" );
	resumeGame->setPosition(ccp(scene->winSize.width/2,scene->winSize.height
		-checkUnitsData->getContentSize().height-resumeGame->getContentSize().height));
	resumeGame->setVisible(false);
	scene->addChild( resumeGame, scene->layerMenu );


	/*passTurn = CCSprite::create( "pass.png" );
	passTurn->setPosition(ccp(scene->winSize.width/2,scene->winSize.height
		-checkUnitsData->getContentSize().height-resumeGame->getContentSize().height-passTurn->getContentSize().height));
	passTurn->setVisible(false);
	scene->addChild( passTurn, scene->layerMenu );*/

	exitGame = CCSprite::create( "exit.png" );
	exitGame->setPosition(ccp(scene->winSize.width/2,scene->winSize.height
		-checkUnitsData->getContentSize().height-resumeGame->getContentSize().height/*-passTurn->getContentSize().height*/-exitGame->getContentSize().height));
	exitGame->setVisible(false);
	scene->addChild( exitGame, scene->layerMenu );


	// units information
	maxHealth = CCLabelTTF::create("Max Health : ", "Arial", 30);
	maxHealth->setPosition(ccp(scene->winSize.width/2,scene->winSize.height-maxHealth->getContentSize().height));
	maxHealth->setVisible(false);
	scene->addChild( maxHealth, scene->layerInformation );

	health = CCLabelTTF::create("Current Health : ", "Arial", 30);
	health->setPosition(ccp(scene->winSize.width/2,scene->winSize.height-health->getContentSize().height*2));
	health->setVisible(false);
	scene->addChild( health, scene->layerInformation );

	ATK = CCLabelTTF::create("Attack : ", "Arial", 30);
	ATK->setPosition(ccp(scene->winSize.width/2,scene->winSize.height-ATK->getContentSize().height*3));
	ATK->setVisible(false);
	scene->addChild( ATK, scene->layerInformation );

	SPEED = CCLabelTTF::create("Speed : ", "Arial", 30);
	SPEED->setPosition(ccp(scene->winSize.width/2,scene->winSize.height-SPEED->getContentSize().height*4));
	SPEED->setVisible(false);
	scene->addChild( SPEED, scene->layerInformation );

	RANGE = CCLabelTTF::create("Range : ", "Arial", 30);
	RANGE->setPosition(ccp(scene->winSize.width/2,scene->winSize.height-RANGE->getContentSize().height*5));
	RANGE->setVisible(false);
	scene->addChild( RANGE, scene->layerInformation );
}