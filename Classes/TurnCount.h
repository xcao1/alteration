#ifndef __TURN_COUNT_H__
#define __TURN_COUNT_H__
#include "State.h"
// count turns and check draw
// if first turn, generate random obstacles
class TurnCount:public State
{
private:
public:
	TurnCount( PlayingScene *scene );
	~TurnCount();

	void onTouchesBegan( CCSet *touches, CCEvent *event );
	void onTouchesMoved( CCSet *touches, CCEvent *event );
	void onTouchesEnded( CCSet *touches, CCEvent *event );
	
	void onUpdate( float dt );
    void onMenu(CCObject* pSender );
};
#endif // !__CHECK_WIN_LOSE_H__
