#include "MenuScene.h"
#include "PlayingScene.h"
using namespace cocos2d;
MenuScene::MenuScene():state(true),printScore(NULL)
{
}
MenuScene::~MenuScene()
{
}
CCScene* MenuScene::scene()
{
    CCScene * scene = NULL;
    // 'scene' is an autorelease object
    scene = CCScene::create();
    if(scene)
	{
		// 'layer' is an autorelease object
		MenuScene *layer = MenuScene::create();
		if( layer)
			scene->addChild(layer);
	}
    return scene;
}
// on "init" you need to initialize your instance
bool MenuScene::init()
{
    bool bRet = false;
    if(! CCLayer::init())
		return bRet;
	int layerBackground = 0;
	int layerMessage = 1;
	CCSize winSize = CCDirector::sharedDirector()->getWinSize();
	CCPoint origin = CCDirector::sharedDirector()->getVisibleOrigin();
	
	float scale;
	mBackground = CCSprite::create("background2.png");
    // position the sprite on the center of the screen
	mBackground->setPosition(ccp(winSize.width/2 + origin.x, winSize.height/2 + origin.y));
	addChild(mBackground, layerBackground);
	scale = winSize.width/mBackground->getContentSize().width;
	mBackground->setScale(scale);

	mName = CCSprite::create("name.png");
    // position the sprite on the center of the screen
	mName->setPosition(ccp(winSize.width/2 + origin.x, 0.7* winSize.height));
	addChild(mName,layerMessage);
	scale = 0.7 * winSize.height/mName->getContentSize().height;
	mName->setScale(scale);


	mGameOver = CCSprite::create("gameover.png");
    // position the sprite on the center of the screen
	mGameOver->setPosition(ccp(winSize.width/2 + origin.x, 0.4*winSize.height));
	addChild(mGameOver,layerMessage);
	mGameOver->setVisible(false);



	mVsAI = CCSprite::create("vsai.png");
	/*scale = 0.2 * winSize.height/mVsAI->getContentSize().height;
	mVsAI->setScale(scale);*/
	mVsAI->setPosition(ccp(winSize.width/2 + origin.x, 0.4*winSize.height ));
	addChild(mVsAI,layerMessage);
	


	mVsHuman = CCSprite::create("vshuman.png");
	/*scale = 0.24 * winSize.height/mVsHuman->getContentSize().height;
	mVsHuman->setScale(scale);*/
	mVsHuman->setPosition(ccp(winSize.width/2 + origin.x, 0.2*winSize.height));
	addChild(mVsHuman,layerMessage);
	



	// print score
	printScore = CCLabelTTF::create("", "Arial", 30);
	if(! printScore)
		return bRet;
	printScore->setPosition(ccp(winSize.width / 2, 0.2*winSize.height));
	printScore->setVisible(false);
	addChild(printScore, layerMessage);
	
	// enable touch interaction
	setTouchEnabled(true);
	schedule( schedule_selector( MenuScene::update ));
	bRet = true;
    return bRet;
}
void MenuScene::setState( bool beginOrEnd )
{
	state = beginOrEnd;
	if( !state )
	{
		mVsHuman->setVisible(false);
		mVsAI->setVisible(false);
		mGameOver->setVisible(true);
		printScore->setVisible(true);
	}
}
void MenuScene::menuCloseCallback(CCObject* pSender)
{
}
void MenuScene::ccTouchesBegan(CCSet *touches, CCEvent *event)
{
}
void MenuScene::ccTouchesMoved(CCSet *touches, CCEvent *event)
{
}
void MenuScene::ccTouchesEnded(CCSet *touches, CCEvent * event)
{
	CCTouch *touch = ( CCTouch * )( touches->anyObject() );
	CCPoint location = touch->locationInView();
	location = CCDirector::sharedDirector()->convertToGL(location);
	
	if(state)
	{// current state : game begin
		CCPoint pos = mVsAI->getPosition();
		CCSize size = mVsAI->getContentSize();
		CCRect rectAI = CCRectMake( pos.x - size.width/2, pos.y - size.height/2, size.width, size.height );
		pos = mVsHuman->getPosition();
		size = mVsHuman->getContentSize();
		CCRect rectHuman = CCRectMake( pos.x - size.width/2, pos.y - size.height/2, size.width, size.height );
		if( rectAI.containsPoint(location) )
		{// vs ai
			CCScene *scene = CCScene::create();
			if(scene)
			{
				// 'layer' is an autorelease object
				PlayingScene *layer = new PlayingScene;
				if( layer)
				{
					layer->vsAI = true;
					if(layer->init())
					{
						scene->addChild(layer);
						// replace scene
						CCDirector::sharedDirector()->replaceScene(scene);
					}
				}
			}
		}
		else if( rectHuman.containsPoint(location) )
		{// vs human
			// start the game
			CCScene *playingScene = PlayingScene::scene();
			CCDirector::sharedDirector()->replaceScene(playingScene);
		}
	}
	else
	{// current state : game over
		// end this application
		CCDirector::sharedDirector()->end();
	}
}

