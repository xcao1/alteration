#ifndef __ACTOR_H__
#define __ACTOR_H__
#include "cocos2d.h"
using namespace cocos2d;
class PlayingScene;
class Actor
{
protected:
	PlayingScene *mScene;
public:
	bool player;
	bool alive;
	int maxHealth;
	int health;
	int recover;
	int ATK;
	int SPEED;
	int RANGE;
	int level;
	int moveChance;
	int attackChance;




	int degree;
	bool selected;
	//float gridSize;
	int gridPosition[2];
	CCSprite *mSprite;
	CCSprite *mSprite2;
	Actor( PlayingScene *scene, int x, int y, bool whichPlayer );
	~Actor();
	bool isAlive();
	bool isActive();
	void updateSpritePosition();
	void moveTo(int x, int y);
	void updateGridMap();
	void onUpdate();
	void showData();
	void hideSprites();
	void removeSprites();
	void refresh();
	void recoverHealth();
	void levelUp();
	void wait();
};
#endif // !__ACTOR_H__
