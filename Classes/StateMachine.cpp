#include "StateMachine.h"
#include "State.h"
#include "TurnCount.h"
#include "UnitSelect.h"
#include "Act.h"
#include "SimpleAI.h"
StateMachine::StateMachine( PlayingScene *scene ):BaseStateMachine(),mScene(scene)
{
	mCurrentState = new TurnCount(scene); 
}
StateMachine::~StateMachine()
{
	delete mCurrentState;
	mCurrentState = NULL;
}


#define SWITCH_TO(NAME,className) \
			case NAME: \
			delete mCurrentState; \
			mCurrentState = new className(mScene); \
			break; 

void StateMachine::switchState()
{
	if( mCurrentState->ended == true )
	{
		MAIN_LOOP name = mCurrentState->nextName;
		switch( name )
		{
			SWITCH_TO( TURNCOUNT, TurnCount);
			SWITCH_TO( UNITSELECT, UnitSelect);
			SWITCH_TO( ACT, Act);
			SWITCH_TO( AI, SimpleAI);
			
		default:
			std::cerr<<"error:no such state.";
			break;
		}
	}
}
void StateMachine::onTouchesBegan( CCSet *touches, CCEvent *event )
{
	switchState();
	mCurrentState->onTouchesBegan( touches, event );
}
void StateMachine::onTouchesMoved( CCSet *touches, CCEvent *event )
{
	switchState();
	mCurrentState->onTouchesMoved( touches, event );
}
void StateMachine::onTouchesEnded( CCSet *touches, CCEvent *event )
{
	switchState();
	mCurrentState->onTouchesEnded( touches, event );
}
void StateMachine::onUpdate( float dt )
{
	switchState();
	mCurrentState->onUpdate( dt );
}
void StateMachine::onMenu(CCObject* pSender )
{
	switchState();
	mCurrentState->onMenu( pSender );
}