#include "Actor.h"
#include "PlayingScene.h"
#include "GridMap.h"
#include "Menu.h"
#include <string>
#include <cstdio>
#include <cstdlib>
using namespace std;
Actor::Actor( PlayingScene *scene, int x, int y, bool whichPlayer ):
	player(whichPlayer),selected(false),degree(0),
	maxHealth(10),health(maxHealth),recover(1),
	ATK(2),SPEED(4),RANGE(1),
	level(1),
	moveChance(1),attackChance(1),
	alive(true),
	mScene(scene)
{
	gridPosition[0] = x;
	gridPosition[1] = y;
	if( player )// player 1
	{
		mSprite = CCSprite::create( "unit1.png" );
		mSprite2 = CCSprite::create( "unit10.png" );
	}
	else
	{
		mSprite = CCSprite::create( "unit2.png" );
		mSprite2 = CCSprite::create( "unit20.png" );
	}
	mScene->mInteractiveScenes->addChild(mSprite, mScene->layerElements);
	mScene->mInteractiveScenes->addChild(mSprite2, mScene->layerElements);
	float scale = mScene->mMap->gridSize/mSprite->getContentSize().width;
	mSprite->setScale(scale);
	mSprite2->setScale(scale);
	mSprite2->setVisible(false);
	updateSpritePosition();
	updateGridMap();
}
Actor::~Actor()
{
}
void Actor::showData()
{
	char value[5];
	string messageString;
	sprintf(value,"%d",maxHealth);
	messageString = "Max Health : "+ string(value);
	mScene->mMenu->maxHealth->setString(messageString.c_str());
	mScene->mMenu->maxHealth->setVisible(true);
	sprintf(value,"%d",health);
	messageString = "Current Health : "+ string(value);
	mScene->mMenu->health->setString(messageString.c_str());
	mScene->mMenu->health->setVisible(true);
	sprintf(value,"%d",ATK);
	messageString = "Attack : " + string(value);
	mScene->mMenu->ATK->setString(messageString.c_str());
	mScene->mMenu->ATK->setVisible(true);
	sprintf(value,"%d",SPEED);
	messageString = "SPEED : " + string(value);
	mScene->mMenu->SPEED->setString(messageString.c_str());
	mScene->mMenu->SPEED->setVisible(true);
	sprintf(value,"%d",RANGE);
	messageString = "RANGE : " + string(value);
	mScene->mMenu->RANGE->setString(messageString.c_str());
	mScene->mMenu->RANGE->setVisible(true);
}
void Actor::moveTo(int x, int y)
{
	if(moveChance>0)
	{
		mScene->mMap->gridMap[gridPosition[0]][gridPosition[1]] = 0;
		moveChance--;
		gridPosition[0] = x;
		gridPosition[1] = y;
		updateGridMap();
	}
}
void Actor::updateGridMap()
{
	if( player )// player 1
		mScene->mMap->gridMap[gridPosition[0]][gridPosition[1]] = 1;
	else// player 2
		mScene->mMap->gridMap[gridPosition[0]][gridPosition[1]] = -1;
}
bool Actor::isActive()
{
	if( moveChance == 0 && attackChance == 0 )
		return false;
	else
		return true;
}
void Actor::wait()
{
	moveChance = 0;
	attackChance = 0;
	selected = false;
}
void Actor::updateSpritePosition()
{
	float diff = mScene->mMap->gridSize - mSprite->getContentSize().width;
	CCPoint position = CCPoint( gridPosition[0]*mScene->mMap->gridSize + mSprite->getContentSize().width/2 + diff/2,
		gridPosition[1]*mScene->mMap->gridSize + mSprite->getContentSize().height/2 + diff/2 );
	mSprite->setPosition( position );
	mSprite2->setPosition( position );
}
bool Actor::isAlive()
{
	if( health <= 0 )
	{
		health = 0;
		alive = false;
	}
	else
	{
		alive = true;
	}
	return alive;
}
void Actor::onUpdate()
{
	if( !isActive() )
	{// switch image
		mSprite->setVisible(false);
		mSprite2->setVisible(true);
	}
	else
	{
		mSprite->setVisible(true);
		mSprite2->setVisible(false);
	}
	updateSpritePosition();
	if( degree > 360 )
		degree = 0;
	if( selected == true )
	{
		degree ++;
		mSprite->setRotation(degree);
	}
	else
		mSprite->setRotation(0);
}
void Actor::hideSprites()
{
	mSprite->setVisible(false);
	mSprite2->setVisible(false);
}
void Actor::removeSprites()
{
	mScene->mInteractiveScenes->removeChild(mSprite, true);
	mScene->mInteractiveScenes->removeChild(mSprite2, true);
	mSprite = NULL;
	mSprite2 = NULL;
}
void Actor::refresh()
{
	moveChance = 1;
	attackChance = 1;
	selected = false;
}
void Actor::recoverHealth()
{
	if(isAlive())
	{
		health += recover;
		if(health > maxHealth)
			health = maxHealth;
	}
}
void Actor::levelUp()
{
	level ++;
	int which = rand()%4;
	if( which == 0 )
		RANGE +=1;
	else if( which == 1 )
		SPEED +=1;
	else if( which == 2 )
		ATK +=1;
	else
		maxHealth +=2;
}