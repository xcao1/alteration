#ifndef __PLAYING_SCENE_H__
#define __PLAYING_SCENE_H__
#include "cocos2d.h"
#include "SimpleAudioEngine.h"
using namespace cocos2d;
class StateMachine;
class GridMap;
class Menu;
class Player;
class PlayingScene:public CCLayer
{
private:
	StateMachine *mStateMachine;
public:
	bool vsAI;
	// device
	CCSize winSize;
	// elements
	Player *one;
	Player *two;


	GridMap *mMap;
	Menu *mMenu;
	// drawables
	
	CCNode *mInteractiveScenes;
	int layerBackground;
	int layerElements;
	int layerInformation;
	int layerMenu;


	// constructor
	PlayingScene();
	// destructor
	~PlayingScene();
	 // there's no 'id' in cpp, so we recommand to return the exactly class pointer
    static CCScene* scene();
    // implement the "static node()" method manually
    CREATE_FUNC(PlayingScene);
    // Here's a difference. Method 'init' in cocos2d-x returns bool, instead of returning 'id' in cocos2d-iphone
    bool init();
	void update( float dt );
	void ccTouchesBegan(CCSet *touches, CCEvent *event);
    void ccTouchesMoved(CCSet *touches, CCEvent *event);
    void ccTouchesEnded(CCSet *touches, CCEvent *event);
	void menuCloseCallback(CCObject* pSender);


	void gameOVer();
};

#endif  // __PLAYING_SCENE_H__