#ifndef __ACT_H__
#define __ACT_H__
#include "State.h"
class Act:public State
{
private:
	bool menuUp;
	enum ACTION 
	{
		None,
		Move,
		Attack,
		Wait
	};
	ACTION mAction;
	Actor *selectedActor;
public:
	Act( PlayingScene *scene );
	~Act();

	void onTouchesBegan( CCSet *touches, CCEvent *event );
	void onTouchesMoved( CCSet *touches, CCEvent *event );
	void onTouchesEnded( CCSet *touches, CCEvent *event );
	void onUpdate( float dt );
    void onMenu(CCObject* pSender );
};
#endif // !__ACT_H__
