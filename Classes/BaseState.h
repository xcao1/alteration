#ifndef __BASE_STATE_H__
#define __BASE_STATE_H__
#include "cocos2d.h"
using namespace cocos2d;
class BaseState
{
public:
	bool ended;
	BaseState():ended(false){}
	~BaseState(){}

	virtual void onTouchesBegan( CCSet *touches, CCEvent *event ) = 0;
	virtual void onTouchesMoved( CCSet *touches, CCEvent *event ) = 0;
	virtual void onTouchesEnded( CCSet *touches, CCEvent *event ) = 0;
	virtual void onUpdate( float dt ) = 0;
    virtual void onMenu(CCObject* pSender ) = 0;
};
#endif // !__BASE_STATE_H__
