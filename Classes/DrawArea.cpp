#include "DrawArea.h"
#include <cmath>
DrawArea::DrawArea( float size ):gridSize(size),range(4),type(0)
{
	position[0] = 0;
	position[1] = 0;
}
DrawArea::~DrawArea()
{
}

void DrawArea::set( int (&pos)[2], int num, int t )
{
	set( pos[0], pos[1], num, t );
}
void DrawArea::set( int x, int y, int num, int t )
{
	position[0] = x;
	position[1] = y;
	range = num;
	type = t;
}
void DrawArea::setGridSize( float size )
{
	gridSize = size;
}
void DrawArea::draw()
{
	ccColor4F color = { 255, 255, 255, 0.35 };
	if( type == 0 )
	{// move
		color.r = 0;color.g = 255; color.b = 0;
	}
	else if( type == 1 )
	{// attack
		color.r = 255; color.g = 0; color.b = 0;
	}
	for( int j = -range; j != range+1; j ++ )
		for( int i = -(range-fabs((float)j)); i != (range-fabs((float)j))+1; i ++ )
			ccDrawSolidRect( ccp((position[0]+ i)*gridSize,(position[1]+j)*gridSize),ccp((position[0]+i+1)*gridSize,(position[1]+j+1)*gridSize),color );
}